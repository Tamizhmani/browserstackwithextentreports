package ExtentReportsDemo;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import BrowserStack.ChromeAndFirefox;

//import BrowserStack.mainTestClass;

class RunnerClass {
	
	ChromeAndFirefox cf= new ChromeAndFirefox();

	public WebDriver driver = null;

	@BeforeClass

	public void setup() throws MalformedURLException {
		
		
		//		DesiredCapabilities caps = new DesiredCapabilities();
		//
		//		//Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
		//		caps.setCapability("browser", "chrome");
		//		caps.setCapability("browser_version", "93.0");
		//		caps.setCapability("os", "Windows");
		//		caps.setCapability("os_version", "8");
		//		caps.setCapability("build", "browserstack-build-1");
		//		caps.setCapability("name", "Thread 1");
		//
		//		driver = new RemoteWebDriver(new URL(URL), caps);
		//		//mainTestClass r1 = new mainTestClass();
		//		//r1.executeTest(capsHashtable);
	}

	//class TestClass2 implements Runnable {
	//	public void run() {
	//		
	//  	}

	@Test 
	public void goToYoutubeChrome() throws MalformedURLException {
		
		driver = cf.setupChrome();
		
		driver.get("https://www.youtube.com/");
		//test.log(PASS, "you tube page launched success");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		WebElement searchBox = driver.findElement(By.xpath("//input[@id='search']"));

		searchBox.sendKeys("Browser Stack Implementation");

		WebElement searchButton =  driver.findElement(By.xpath("//button[@id='search-icon-legacy']"));

		searchButton.click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
	}
	
	@Test 
	public void goToYoutubeFirefox() throws MalformedURLException {
		
		driver = cf.setupFirefox();

		driver.get("https://www.youtube.com/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		WebElement searchBox = driver.findElement(By.xpath("//input[@id='search']"));

		searchBox.sendKeys("Browser Stack Implementation");

		WebElement searchButton =  driver.findElement(By.xpath("//button[@id='search-icon-legacy']"));

		searchButton.click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@AfterSuite

	public void tearDown() {

		driver.quit();

	}
}
//}