package BrowserStack;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ChromeAndFirefox {
	
	//public static final String URL = null;
	public static DesiredCapabilities caps1,caps2 ;
	public static WebDriver driver = null;
	
	public static final String USERNAME = "tamizhmani_1Ogj6W";
	public static final String AUTOMATE_KEY = "AoX5bN4FaCFift2texvD";
	public static final String URL  = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	
	public  WebDriver  setupChrome() throws MalformedURLException {

		 caps1 = new DesiredCapabilities();
		 
		 //Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
		caps1.setCapability("browser", "chrome");
		caps1.setCapability("browser_version", "93.0");
		caps1.setCapability("os", "Windows");
		caps1.setCapability("os_version", "8");
		caps1.setCapability("build", "browserstack-build-1");
		caps1.setCapability("name", "Thread 1");
		

		return driver = new RemoteWebDriver(new URL(URL), caps1);
		
	}
	
	public  WebDriver  setupFirefox() throws MalformedURLException {
		
		 caps2 = new DesiredCapabilities();

		 System.out.println("testing");
		 //Hashtable<String, String> capsHashtable = new Hashtable<String, String>();
		 caps2.setCapability("browser", "firefox");
		 caps2.setCapability("browser_version", "85.0");
		 caps2.setCapability("os", "Windows");
		 caps2.setCapability("os_version", "8");
		 caps2.setCapability("build", "browserstack-build-1");
		 caps2.setCapability("name", "Thread 2");
			

		return driver = new RemoteWebDriver(new URL(URL), caps2);
	}

}
