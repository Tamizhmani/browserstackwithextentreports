package Listeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

/** ITestListener is an interface implemented in the class , and that class overrides the 
    ITestListener defined methods. The ITestListener listens to the desired events 
    and executes the methods accordingly

 */
public class Listener implements ITestListener{

	//Extent Reports Declaration
	private static ExtentReports extent = ExtentManager.createInstance();
	private static ThreadLocal<ExtentTest> test =new ThreadLocal<>();


	/* Synchronized methods are not executed b two threads at same time*/
	/*  ITestContext is a class that contains information about the test run*/
	@Override
	public synchronized void onStart(ITestContext context) {
		System.out.println("EXTENT REPORTS TEST SUITE STARTED..");
	}

	/*ITestResult is an interface that defines the result of the test*/
	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println(result.getMethod().getMethodName() + " ---- STARTED");
		ExtentTest extentTest = extent.createTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
		test.set(extentTest);
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println(result.getMethod().getMethodName() + " ---- PASSED");
		test.get().pass("TEST PASSED");

	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println(result.getMethod().getMethodName() + " ---- FAILED");
		test.get().fail(result.getThrowable().getMessage());

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println(result.getMethod().getMethodName() + " ---- SKIPPED");
		test.get().skip(result.getThrowable().getMessage());

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		System.out.println("EXTENT REPORTS TEST SUITE ENDED..");
		extent.flush();
	}


}
