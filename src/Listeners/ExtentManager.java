package Listeners;

import java.io.File;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {
	
	private static ExtentReports extentReport;
	public static String reportName = "report.html";
	public static String FilePath = System.getProperty("user.dir")+"/TestReport/";
	public static String FileName = FilePath+reportName;
	public static ExtentHtmlReporter htmlreporter;

	public static ExtentReports createInstance() {
		
		// TODO Auto-generated method stub
		
		//File file = new File (System.getProperty("user.dir")+"/TestReport/"+reportName);
		htmlreporter = new ExtentHtmlReporter (System.getProperty("user.dir")+"\\TestReport\\"+reportName);
		
		htmlreporter.config().setTheme(Theme.DARK);;
		htmlreporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlreporter.config().setChartVisibilityOnOpen(true);
		htmlreporter.config().setDocumentTitle("EXTENT EPORT FOR BROWSER STACK");
		htmlreporter.config().setEncoding("utf-8");
		
		extentReport = new ExtentReports();
		
		extentReport.attachReporter(htmlreporter);
		
		return extentReport;
	}

}
